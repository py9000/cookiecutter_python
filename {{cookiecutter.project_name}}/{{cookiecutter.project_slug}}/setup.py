import sys
from setuptools import setup, find_packages

from {{cookiecutter.project_slug}}.settings import VERSION, MINIMUM_PYTHON_VERSION

assert sys.version_info >= MINIMUM_PYTHON_VERSION

setup(
    name="python-sample-project",
    version=VERSION,
    description="python-sample-project",
    url="https://gitlab.com/terminallabs/sample-project/sample-project_python",
    author="Terminal Labs",
    author_email="solutions@terminallabs.com",
    license="mit",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        "setuptools",
        "utilities-package@git+https://gitlab.com/terminallabs/"
        "utilitiespackage/utilities-package.git"
        "@master#egg=utilitiespackage&subdirectory=utilitiespackage",
    ],
    entry_points="""
      [console_scripts]
      {{cookiecutter.project_slug}}={{cookiecutter.project_slug}}.__main__:main
  """,
)
